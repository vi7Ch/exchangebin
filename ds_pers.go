
## mysql

package main

import (
	"database/sql"
	"fmt"
	"log"
	"time"

	_ "github.com/go-sql-driver/mysql"
)

func main() {
	// Replace the connection parameters with your MySQL configuration
	dsn := "user:password@tcp(127.0.0.1:3306)/database"

	// Open a connection to the database
	db, err := sql.Open("mysql", dsn)
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	// Ping the database to check the connection
	if err := db.Ping(); err != nil {
		log.Fatal(err)
	}

	// Your SQL query
	query := "SELECT * FROM your_table"

	// Measure the time it takes to execute the query
	startTime := time.Now()

	// Execute the query
	rows, err := db.Query(query)
	if err != nil {
		log.Fatal(err)
	}
	defer rows.Close()

	// Process the result if needed
	// For example, you can iterate through rows with rows.Next()

	// Measure the response time
	responseTime := time.Since(startTime)
	fmt.Printf("Query executed in %s\n", responseTime)
}


---- with profiling

package main

import (
	"database/sql"
	"fmt"
	"log"

	_ "github.com/go-sql-driver/mysql"
)

func main() {
	// Replace the connection parameters with your MySQL configuration
	dsn := "user:password@tcp(127.0.0.1:3306)/database"

	// Open a connection to the database
	db, err := sql.Open("mysql", dsn)
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	// Ping the database to check the connection
	if err := db.Ping(); err != nil {
		log.Fatal(err)
	}

	// Enable profiling
	_, err = db.Exec("SET profiling = 1")
	if err != nil {
		log.Fatal(err)
	}

	// Your SQL query
	query := "SELECT * FROM your_table"

	// Execute the query
	rows, err := db.Query(query)
	if err != nil {
		log.Fatal(err)
	}
	defer rows.Close()

	// Process the result if needed
	// For example, you can iterate through rows with rows.Next()

	// Display profiles
	var profileID, queryDuration, queryType, queryStatus string
	err = db.QueryRow("SHOW PROFILES").Scan(&profileID, &queryDuration, &queryType, &queryStatus)
	if err != nil {
		log.Fatal(err)
	}

	fmt.Printf("Profile ID: %s\n", profileID)
	fmt.Printf("Query Duration: %s\n", queryDuration)
	fmt.Printf("Query Type: %s\n", queryType)
	fmt.Printf("Query Status: %s\n", queryStatus)
}


## postgres

package main

import (
	"database/sql"
	"fmt"
	"log"
	"time"

	_ "github.com/lib/pq"
)

func main() {
	// Replace the connection parameters with your PostgreSQL configuration
	dsn := "user=username dbname=mydb sslmode=disable"

	// Open a connection to the database
	db, err := sql.Open("postgres", dsn)
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	// Ping the database to check the connection
	if err := db.Ping(); err != nil {
		log.Fatal(err)
	}

	// Enable the pg_stat_statements extension
	_, err = db.Exec("CREATE EXTENSION IF NOT EXISTS pg_stat_statements")
	if err != nil {
		log.Fatal(err)
	}

	// Your SQL query
	query := "SELECT * FROM your_table"

	// Measure the response time
	startTime := time.Now()

	// Execute the query
	rows, err := db.Query(query)
	if err != nil {
		log.Fatal(err)
	}
	defer rows.Close()

	// Process the result if needed
	// For example, you can iterate through rows with rows.Next()

	// Display response time
	duration := time.Since(startTime)
	fmt.Printf("Query Response Time: %s\n", duration)
}


## mssql

package main

import (
	"database/sql"
	"fmt"
	"log"
	"time"

	_ "github.com/denisenkom/go-mssqldb"
)

func main() {
	// Replace the connection parameters with your SQL Server configuration
	connString := "server=your_server;user id=your_user;password=your_password;database=your_database"

	// Open a connection to the database
	db, err := sql.Open("sqlserver", connString)
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	// Ping the database to check the connection
	if err := db.Ping(); err != nil {
		log.Fatal(err)
	}

	// Your SQL query
	query := "SELECT * FROM your_table"

	// Measure the response time
	startTime := time.Now()

	// Execute the query
	rows, err := db.Query(query)
	if err != nil {
		log.Fatal(err)
	}
	defer rows.Close()

	// Process the result if needed
	// For example, you can iterate through rows with rows.Next()

	// Display response time
	duration := time.Since(startTime)
	fmt.Printf("Query Response Time: %s\n", duration)
}


## oracle

package main

import (
	"context"
	"database/sql"
	"fmt"
	"log"
	"time"

	"github.com/sijms/go-ora/v2"
)

func main() {
	// Replace the connection parameters with your Oracle configuration
	connString := "user/password@host:port/service_name"

	// Open a connection to the database
	db, err := sql.Open("ora", connString)
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	// Ping the database to check the connection
	if err := db.Ping(); err != nil {
		log.Fatal(err)
	}

	// Your SQL query
	query := "SELECT * FROM your_table"

	// Measure the response time
	startTime := time.Now()

	// Execute the query
	rows, err := db.Query(query)
	if err != nil {
		log.Fatal(err)
	}
	defer rows.Close()

	// Process the result if needed
	// For example, you can iterate through rows with rows.Next()

	// Display response time
	duration := time.Since(startTime)
	fmt.Printf("Query Response Time: %s\n", duration)
}




